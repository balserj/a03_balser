<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="styles.css">
<title>
  Assignment 03 -- Balser, Justin
</title>
</head>

<div id="Top">
  <h1>Assignment 03 -- Balser, Justin</h1>
</div>

<header>
<a href="#Shopping List">Grocery List</a>
<a href="#States">States List</a>
<a href="#Movies">Movies</a>
<a href="#Dog Breeds">Breed Stats</a>
<a href="#State Info">State Info</a>
<a href="#Meals">Meals</a>
</header>

<body>

<div id="Shopping List">
<h2>Grocery List:</h2>
<ul>
  <li>Cheese</li>
  <li>Milk</li>
  <li>Bananas</li>
  <li>Bread</li>
  <li>Yogurt</li>
</ul>
</div>

<a href="#Top">Back to Top</a>
<br>

<div id="States">
<h2>State and City List</h2>
<ul>
  <li>Ohio</li>
    <ol>
      <li>Columbus</li>
      <li>Cincinnati</li>
      <li>Akron</li>
      <li>Kent</li>
    </ol>
  </li>

  <li>West Virginia</li>
    <ol>
      <li>Ravenswood</li>
      <li>Ripley</li>
      <li>Charleston</li>
      <li>Morgantown</li>
    </ol>
  </li>

  <li>Pennsylvania</li>
    <ol>
      <li>Scranton</li>
      <li>Philadeplhia</li>
      <li>Pittsburgh</li>
      <li>Gettysburg</li>
    </ol>
  </li>

  <li>North Carolina</li>
    <ol>
      <li>Charlotte</li>
      <li>Raleigh</li>
      <li>Asheville</li>
      <li>Greensboro</li>
    </ol>
  </li>

</ul>
</div>

<a href="#Top">Back to Top</a>

<br>

<div id="Movies">
<h2>Favorite Movies</h2>
<dl>
<dt>Hunger Games</dt>
<dd>In what was once North America now stands the Capital of Panem which controls each of its 12 districts by selecting 1 boy and 1 girl each year
to compete and survive against each other in the Hunger Games. A girl named Katniss Everdeen from district 12 offers herself as tribute for her sister
who was selected and his to fight and survive no matter the cost.</dd>
<dt>Percy Jackson and the Lightning Thief</dt>
<dd>A troublesome teenager named Percy Jackson discovers he is a demigod and son of Poseidon and gets taken to a camp for other demigods like him.
Once there, he trains to improve his skills and powers and has to stop a feud between the gods because someone stole Zeus's thunderbolt as well
as saving his mother from Hades before its too late.</dd>
<dt>Back to the Future</dt>
<dd>In the year of 1985, a teenager Marty McFly takes a time machine, made by his scientist friend Dr. Brown, back to 1955 and has to make sure
his parents fall in love, because if he fails, he and his siblings will cease to exist. He also needs to get back to his own time and save Dr. Brown.</dd>
<dt>Despicable Me</dt>
<dd>A supervillain named Gru is developing his greatest heist yet, to steal the moon. To do so, he needs to steal a shrink ray, however one of
his greatest competitirs named Vector steals the shrink ray from Gru so Gru has to use adopted orphans to steal back the shrink ray to proceed
in stealing the moon. However, Vector steals the orphans and Gru finds out the moon wont stay shrunk so he tries to save the kids from Vector
before the moon grows back to normal size and takes out everyone.</dd>
<dt>Madagascar</dt>
<dd>A group of 4 animals, Alex the lion, Marty the zebra, Melman the giraffe and Gloria the hippo escape the Central Park Zoo in New York
and end up lost at sea and land on Madagascar and have to survive and hope for others to come save them. They have to learn how to adapt
to the jungle and the other creatures who live there to stay alive and have a chance to make it back to civilization.</dd>
</dl>
</div>

<a href="#Top">Back to Top</a>

<br>

<div id="Dog Breeds">
<table align="center">

  <tr>
      <th colspan="5">Breed Stats</th>
  </tr>

  <tr>
    <th></th>
    <th>Retrievers</th>
    <th>Bulldogs</th>
    <th>German Shorthaired</th>
    <th>Boxers</th>
  </tr>

  <tr>
      <th>Average Height</th>
      <td>2 ft.</td>
      <td>12-16 in.</td>
      <td>23 in.</td>
      <td>22-24 in.</td>
  </tr>

  <tr>
      <th>Average Weight</th>
      <td>65 lbs</td>
      <td>50 lbs</td>
      <td>60 lbs</td>
      <td>60 lbs</td>
  </tr>

  <tr>
      <th>Example Breed</th>
      <td>Golden Retriever</td>
      <td>Olde English Bulldogge</td>
      <td>Pointer</td>
      <td>American Boxer</td>
  </tr>
</table>
</div>

<a href="#Top">Back to Top</a>

<br>

<div id="State Info">
<table align="center"  id="StateData">
    <tr>
      <th colspan="6">State Info</th>
    </tr>

    <tr>
        <th>State Name</th>
        <td>Ohio</td>
        <td >West Virginia</td>
        <td>Pennsylvania</td>
        <td>Minnesota</td>
        <td>Georgia</td>
    </tr>

    <tr>
        <th>State Bird</th>
        <td>Northern Cardinal</td>
        <td>Cardinal</td>
        <td>Ruffed Grouse</td>
        <td>Loon</td>
        <td>Brown Thrasher</td>
    </tr>

    <tr>
        <th>State Flower</th>
        <td>Carnation</td>
        <td>Rhododendron</td>
        <td>Mountain-laurel</td>
        <td>Showy Lady's Slippers</td>
        <td>Rosa laevigata</td>
    </tr>

    <tr>
        <th>State Tree</th>
        <td>Ohio Buckeye Tree</td>
        <td>Sugar Maple</td>
        <td>Eastern Hemlock</td>
        <td>Red Pine</td>
        <td>Southern Live Oak</td>
    </tr>
</table>
</div>

<a href="#Top">Back to Top</a>

<br>

<div id="Meals">
<table align="center">
  <tr>
    <th colspan="5" style="background-color: #FFB833; color:SaddleBrown">Meals</th>
  </tr>

  <tr id="test">
    <th colspan="2"></th>
    <th><b>Breakfast<b></th>
    <th><b>Lunch</b></th>
    <th><b>Dinner</b></th>
  </tr>

  <tr>
      <th rowspan="4">Foods</th>
          <th>Bread</th>
          <td>Roll</td>
          <td>Sliced Bread</td>
          <td style="background-color: hsla(120,100%,50%,0.3);color: hsl(0, 51%, 33%)">Garlic Bread</td>
  </tr>

  <tr>
    <th>Main Course</th>
    <td>Eggs</td>
    <td>Ham Sandwich</td>
    <td style="background-color: hsla(120,100%,50%,0.3);color: hsl(0, 51%, 33%)">Spaghetti</td>
  </tr>

  <tr>
    <th>Vegetable</th>
    <td>Tomato</td>
    <td>Salad</td>
    <td style="background-color: hsla(120,100%,50%,0.3);color: hsl(0, 51%, 33%)">Brussel Sprouts</td>
  </tr>

  <tr>
    <th>Dessert</th>
    <td>N/A</td>
    <td>Cookies</td>
    <td style="background-color: hsla(120,100%,50%,0.3);color: hsl(0, 51%, 33%)">Ice Cream</td>
  </tr>
</table>
</div>

<a href="#Top">Back to Top</a>

</body>
</html>
